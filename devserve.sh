#!/usr/bin/env sh
# SPDX-License-Identifier: MIT-0
# scspell-id: 12960ecc-aad2-11ee-89ea-80ee73e9b8e7
# Copyright (c) 2022-2023 The DPS8M Development Team

##########################################################################

set -eu

##########################################################################

${HUGO:-hugo} server     \
  -b "http://127.0.0.1/" \
  --bind 0.0.0.0         \
  -D                     \
  --disableFastRender    \
  --ignoreCache          \
  --noHTTPCache          \
  -p 31313

##########################################################################
