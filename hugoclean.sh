#!/usr/bin/env sh
# SPDX-License-Identifier: MIT-0
# scspell-id: 200bfb5c-aad2-11ee-872e-80ee73e9b8e7
# Copyright (c) 2022-2023 The DPS8M Development Team

##########################################################################

set -eu

##########################################################################

test -f "config.toml" ||
  {
	printf '%s\n' "ERROR: 'config.toml' not found."
	exit 1
  }

##########################################################################

rm -rf ./resources > /dev/null 2>&1 || true
rm -rf ./public    > /dev/null 2>&1 || true

##########################################################################
