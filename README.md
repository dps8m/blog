<!-- SPDX-License-Identifier: MIT-0 -->
<!-- scspell-id: 3f5c0542-aafa-11ee-b31f-80ee73e9b8e7 -->
<!-- Copyright (c) 2006-2024 The DPS8M Development Team -->

The DPS8M Blog is based on the Geekblog theme for Hugo: https://github.com/thegeeklab/hugo-geekblog

The source code is licensed under the MIT License; see the [LICENSE](LICENSES/MIT.txt) file for details.

Note the used SVG icons and generated icon fonts are licensed under the license of the respective icon pack:
- Font Awesome: [CC-BY-4.0](https://github.com/FortAwesome/Font-Awesome#license)
- IcoMoon Free Pack: [GPL / CC-BY-4.0](https://icomoon.io/#icons-icomoon)
- Material Icons: [Apache 2.0](https://github.com/google/material-design-icons/blob/main/LICENSE)
