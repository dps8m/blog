#!/usr/bin/env sh
# SPDX-License-Identifier: MIT-0
# scspell-id: 1c78752e-aad2-11ee-b22e-80ee73e9b8e7
# Copyright (c) 2022-2023 The DPS8M Development Team

##########################################################################

set -eu

##########################################################################

test -f "config.toml" ||
  {
	printf '%s\n' "ERROR: 'config.toml' not found."
	exit 1
  }

##########################################################################

test -f "hugoclean.sh" ||
  {
	printf '%s\n' "ERROR: 'hugoclean.sh' not found."
	exit 1
  }

##########################################################################

# shellcheck disable=SC1091
./hugoclean.sh

##########################################################################

${HUGO:-hugo}                         \
  -b "https://dps8m.gitlab.io/blog"   \
  --logLevel info

##########################################################################
