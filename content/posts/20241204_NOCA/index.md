---
title: Proposal：Simulator-aware extensions for Multics
type: posts
date: 2024-12-04
tags:
  - Multics
authors:
  - 'Charles Anthony'
resources:
  - name: cover
    src: "images/rsw.png"
    title: rsw
---
<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!-- Copyright (c) 2016-2024 The DPS8M Development Team -->

**Charles Anthony has proposed extensions to [Multics](https://swenson.org/multics_wiki/) to enable enhanced performance and functionality when Multics is executing in the [simulated environment](https://dps8m.gitlab.io/).**

This post aims to gather early feedback from relevant stakeholders and interested parties to reach a consensus before drafting and submitting an official [MCR](https://multicians.org/mgm.html#MCR). Therefore, the following proposal should be considered to be *suggestions* and are intended as *discussion points*, not prescriptions.

### History

* <small>Revision 1.00 (2024-12-04) - Initial posting</small>
* <small>Revision 1.01 (2024-12-05) - Minor corrections and formatting</small>
* <small>Revision 1.02 (2024-12-06) - Add simulator_extensions section</small>
* <small>Revision 1.03 (2024-12-08) - "Enhanced" verbiage; add "Motivations" section</small>

### Motivations

Running Multics on simulated or newly developed hardware offers the opportunity to modify the behavior of the system and add features to Multics to leverage those modifications to improve or enhance Multics.

### Constraints

Any change to Multics to make Multics simulator aware must preclude any loss of compatibility with current ([*and future*](https://dps8m.gitlab.io/blog/tags/FPGA/)) non-simulated systems.

Two guidelines to manage this are suggested:

1. Multics must be able to reliably determine that it is running on a simulator (or enhanced hardware).

2. Any changes to Multics behavior should require explicit action by the operator, and the implementation of that  behavioral change must verify that it is running on a simulator, falling back to the preexisting behavior if not running on a simulator.

### Determining simulator status

Using the *Configuration Switch Data* "***Processor Type***" field is suggested as the test for determining if Multics is running on a simulator.

Specifically, the *RSW* instruction with an operand address of the form `xxxxx2` currently returns a 2 bit field in bits 4 to 5.

* For *Level 68* CPUs, that field is defined as (see [***AL39***, *pg. 68*](http://www.bitsavers.org/pdf/honeywell/large_systems/multics/AL39.pdf#%5B%7B%22num%22%3A202%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C54%2C756%2C0%5D)):
  ```text
          Equals “00” for a L68 or a DPS processor.
  ```

[]()

* For *DPS‑8/M* CPUs, that field is defined as (see [***AL39***, *pg. 70*](http://www.bitsavers.org/pdf/honeywell/large_systems/multics/AL39.pdf#%5B%7B%22num%22%3A208%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C54%2C362.6%2C0%5D)):
  ```text
        Indicates processor type
               00 = L68 or DPS Processor
               01 = DPS‑8/M Processor
               10 = Reserved for future use
               11 = Reserved for future use
  ```

[]()

The extension is proposed as follows:

* Based on the assumption that the owners of that field are never going to use these reserved values, assigning:
  * '`10`' as a "**Enhanced Level‑68 or DPS Processor**" and,
  * '`11`' as a "**Enhanced DPS‑8/M Processor**",
  will serve as a reliable test of an enhanced processor or simulation status.

[]()

* To determine additional information about the enhanced processor, note that `RSW` operand values of `xxxxx0`, `xxxxx1`, `xxxxx2`, `xxxxx3`, and `xxxxx4` are the only defined values; it is therefore suggested that `xxxxx7` be assigned an `RSW` operand value to access enhanced features.

[]()

* The following format for the field is suggested:
  ```text
          Bit 0: Always 1.

          Bit 1-3: Page and Segment cache mode:
              000: Page and Segment Descriptor caches behave normally.
              001: Page and Segment caches do not exist – the CAMS and
                   CAMP are NOPs, and Page/Segment table changes do
                   not need to be signaled.
              Other values: Reserved for future use.

          Bit 4-35: Set to 0; reserved for future use.
  ```

[]()

In order to provide a robust test bed for Multics development, it is also suggested that these extensions (`RSW` processor type, `RSW` operand `xxxxx7`) be enabled or disabled via run-time configuration options in the **DPS8M Simulator**.

[]()

* For a per-CPU option:
  ```text
          set cpu0 config=enhanced_extensions={on|off}
  ```
* For a system-wide option:
  ```text
          set sys config=enhanced_extensions={on|off}
  ```
* Configuration options should also be invented to allow enabling or disabling individual extensions, when `enhanced_extensions` are enabled generally.

[]()

If `enhanced_extensions` is **off**, the `RSW` instruction will behave as documented in ***AL39***; if **on**, the processor type bits and `RSW` `xxxxx7` will behave as as described above.

### NOCA：A proposed Multics extension

As noted above, the Page and Segment Descriptor caches are typically not used in the simulated processor due to the excessive implementation overhead of simulated content addressable memory.

In order to preserve cache coherency in multiple CPU configurations, any change to the Page or Segment Descriptors has Multics ***1)*** marking the affected process as suspended, ***2)*** setting a table lock, ***3)*** updating the descriptors, ***4)*** signaling each of the other CPUs to flush their descriptor caches, ***5)*** waiting for each CPU to acknowledge the flush, ***6)*** releasing the lock, and ***7)*** resuming the suspended process; this is a time-consuming process made unnecessary in the simulated environment due to the lack of actual caching.

[]()

* The configuration deck "`PARM` card" is an ideal place for controlling Multics extensions such as setting cache consistency policies; for example:
  ```text
          PARM NOCA
  ```
  would set a flag instructing Multics to skip the entire cache flush process.

[]()

* The configuration deck validation code for the `NOCA` option would check that Multics is actually running on a simulated system and that all of the configured CPUs are running the no-cache mode, and generate a warning along the lines of ***`NOCA not supported in the current execution environment`***, and then proceed normally.

[]()

In the code that implements the cache coherency process, a simple check of the `NOCA` flag would allow the procedure to do any needed house-cleaning and then return.

(A more sophisticated implementation would track a *per-CPU* `NOCA` flag, and only execute the cache coherency code if a running processor was marked as not supporting `NOCA`, only signaling those processors. In a scenario where Multics is running in a *mixed* real-hardware (*e.g.* enhanced FPGA implementation) and simulated-hardware environment, this becomes an interesting project.)

### Conclusion

* I believe that ***a)*** it is feasible in general to implement Multics simulator-aware extensions in a way that embraces both the historical artifact value and the design principles of Multics; and, ***b)*** that these extensions will provide value to the Multics community.

### Links

* Follow-ups should be posted to the [**dps8m-developers**](https://sourceforge.net/p/dps8m/mailman/dps8m-developers/thread/517a8646-7745-4c9f-964a-6965b2b2dceb%40app.fastmail.com/#msg58849724) mailing list.
  * Please [**subscribe**](https://sourceforge.net/projects/dps8m/lists/dps8m-developers) to the list prior to posting.
* Real-time discussions are hosted on the ["Multiplexed Information and Computing Service" Slack workspace](https://dps8m.gitlab.io/dps8m/Community/#slack).
  * Join the [#**multics**](https://multics-os.slack.com/archives/CBYL52N76) channel or [request access](https://join.slack.com/t/multics-os/shared_invite/zt-1ejykw0eu-mHHr0_FS8o4GOeAVXKmQew) to the Slack workspace.

—&nbsp;*Charles Anthony*
