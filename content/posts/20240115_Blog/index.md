---
title: Welcome to the DPS8M Blog
weight: 1
type: posts
date: 2024-01-15
tags:
  - Announcements
authors:
  - 'DPS8M Development Team'
resources:
  - name: A1
    src: "images/A1.png"
  - name: A2
    src: "images/A2.png"
  - name: A3
    src: "images/A3.png"
---
<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!-- Copyright (c) 2016-2024 The DPS8M Development Team -->

**The DPS8M Development Team** officially announces the new [**DPS8M Blog**](https://dps8m.gitlab.io/blog/).

The **DPS8M Blog** will cover all topics surrounding the [Honeywell 6000-series](https://en.wikipedia.org/wiki/Honeywell_6000_series) and the [**DPS8M Simulator**](https://dps8m.gitlab.io/dps8m/).

You can expect posts from our developers about both hardware and software, with a particular focus on the **Multics** system (both [**current**](https://swenson.org/multics_wiki/) and [**historical**](https://multicians.org/)), but we'll also touch on [**GCOS-3**](https://en.wikipedia.org/wiki/General_Comprehensive_Operating_System#Honeywell_GCOS_3) and [**CP-6**](https://en.wikipedia.org/wiki/Honeywell_CP-6), as well as adjacent Honeywell technologies such as the 18-bit [**DATANET**](http://www.bitsavers.org/pdf/datapro/communications_processors/C13-480_Honeywell_DATANET_6661.pdf)/[**355**](http://www.bitsavers.org/pdf/honeywell/large_systems/datanet/DATANET_355_Brochure.pdf) line of network processors, and even the Honeywell [**Level 6**](https://en.wikipedia.org/wiki/Honeywell_Level_6) minicomputers and the [**Goodyear STARAN**](https://en.wikipedia.org/wiki/STARAN) or [**Cray**](https://multicians.org/mulimg/userring-02.jpg) systems that were often paired with Honeywell large systems for scientific computing.

[]()
[]()

| | | |
|-|-|-|
| {{< img name="A1" lazy="true" alt="" size="profile" >}} | {{< img name="A3" lazy="true" alt="" size="profile" >}} | {{< img name="A2" lazy="true" alt="" size="profile" >}} |

[]()
[]()

You can follow our blog [here](https://dps8m.gitlab.io/blog/) on the web, or subscribe using the {{< icon "gblog_rss_feed" >}} [Atom](http://dps8m.gitlab.io/blog/feed.xml) feed with your favorite newsreader application.

—&nbsp;*The DPS8M Development Team*
