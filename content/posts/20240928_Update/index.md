---
title: DPS8M R3.0.2 is coming soon ...
type: posts
date: 2024-09-28
tags:
  - Progress
authors:
  - 'Jeffrey H. Johnson'
---
<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!-- Copyright (c) 2016-2024 The DPS8M Development Team -->

**DPS8M R3.0.2 is coming soon.**

**DPS8M R3.0.2** will be an incremental release that includes bug fixes and performance enhancements developed over the last year, and is intended to get these improvements into the hands of our users before the release of the next major version of the simulator, which will include new features (*and potentially breaking changes*).

You can review the (*in progress*) [R3.0.2 release notes](https://gitlab.com/dps8m/dps8m/-/wikis/DPS8M-R3.0.2-Release-Notes) for a more detailed list of changes, but read on for some highlights.

## New features

**DPS8M R3.0.2** will be a minor release, and will *not* incorporate new features or functionality, with three exceptions:

1. The `PANEL68` build option is included in the main source tree.  This supports the *hardware* Level-68 maintenance panel.  Since there is currently *one* known panel in the wild - the one previously installed at the *Living Computers Museum + Labs*, now at the *SDF* - 99.9% of users will be unaffected.

2. The [`libbacktrace`](https://github.com/ianlancetaylor/libbacktrace) library is included in the main source tree as a convenience.  It's use is completely optional and *might* improve crash reporting details (without requiring the use of a debugger by the user).

3. The [`libsir`](https://github.com/aremmell/libsir) library is included in the main source tree.  It is currently not used by the main simulator, but will be *extensively* integrated in the next major release.  We've been working with *libsir* upstream extensively over the last year to ensure that it works on a large *superset* of the compilers and operating systems that **DPS8M** currently supports, and in the long run, it will actually help **DPS8M** to be more easily portable in the future.

## Portability enhancements

The upcoming release will include some portability enhancements over previous versions:

* [Android](https://dps8m.gitlab.io/blog/posts/20240322_DPS8M_Android/) builds that are cross-compiled using the NDK will now work out-of-the-box on retail devices.

* [IBM i (OS/400)](https://dps8m.gitlab.io/blog/posts/20240207_DPS8M_AS400/) is officially supported.

* Fixed some warnings when building with **Clang 19**, **GCC 14**, and current **Cygwin**.

## Performance improvements

The upcoming release will have *excellent* performance improvements for *all* users of the simulator:

* Linux performance improved by **10**-**15**%.

* macOS performance improved by **50**-**60**%.

* Haiku performance improved by **65**-**75**%.

* Clang compiler build performance (*on all platforms*) improved by **15**-**20**%.

* On FreeBSD systems, the shared state will be mapped using `MAP_NOSYNC` to reduce unnecessary disk IO.

### Profile-guided optimization (PGO)

The upcoming release will introduce some (*experimental*) scripts for building the simulator using profile guided optimization.

These scripts are designed to run from the root of a clone of the `git` repository.  Our initial benchmarking shows gains of up to **40**% are possible, but we expect most users to see improvements of about **15**-**20**%.

Currently, the profile run is quite naïve, but will be improved before the next major release.  Because of the possibility of PGO builds having *worse* performance than normal optimized release builds, we encourage you run your own benchmarks using your typical workloads before using PGO builds in production.

* On most Unix-like systems (*e.g.* Linux, FreeBSD, Haiku):
  * `./src/pgo/Build.PGO.Clang.sh` (using Clang)
  * `./src/pgo/Build.PGO.GCC.sh` (using GCC)

* On macOS systems (with Homebrew's `llvm` or `gcc` installed):
  * `./src/pgo/Build.PGO.Homebrew.Clang.sh` (using Clang)
  * `./src/pgo/Build.PGO.Homebrew.GCC.sh` (using GCC)

## Toolchain modernization

Progress marches on.  New operating systems and compiler versions are released and older software reaches end-of-life.  Old bugs are fixed and new bugs are created.

Although most of our users are technically proficient, not all of them are UNIX (or Mac or Windows) or compiler experts.  Some of our users know the Honeywell system better the modern computer hosting the simulator.  Others might be students, who shouldn't need to learn about *two* operating systems *and* software development, all at once, just to use **DPS8M**.

Starting with the release of **DPS8M R1.0.0** in 2017, **The DPS8M Development Team** has released pre-compiled binary packages for all popular (and some less popular) platforms for the benefit of our users, so they don't have to be experts in *building* software to simply *use* it.  While **DPS8M** may not be a typical end-user application, you don't need to be a software developer to get started.

Providing binary releases benefits our developers as well.  Because we *extensively* test the toolchains used to build each release, and then further certify the proper functioning and stability of the resulting binary, we can be confident that any reported bugs are legitimate and not the result of faulty build tools.

*Unfortunately, it's simply not sustainable for us to continue to generate and fully test the many custom toolchains required, and also perform the testing necessary to certify, on real hardware, the resulting binary releases, for all the platforms that we currently support.*

* Beginning with **DPS8M R3.0.2**, we will **no longer** automatically build bleeding edge binaries for some less popular and esoteric platforms.  These include binaries for AIX/POWER6, Linux/MIPS, Linux/m68k, Linux/OpenRISC, Linux/SuperH, etc.  Other binaries, such as those targeting NetBSD, OpenBSD, and DragonFly BSD, will *only* be built for stable releases. *We are <b>not</b> dropping the support for any of these platforms*, just the pre-compiled bleeding edge binary builds.

* By eliminating pre-compiled binary builds used by *very* few (and often *zero*) users, we shorten our CI/CD turnaround times and reduce our manual testing load, enabling us to make more frequent stable releases.

We expect these changes will affect *very* few actual users.

The following table summarizes the changes in the toolchains utilized by **The DPS8M Development Team** for CI/CD, bleeding edge, and stable builds of the simulator since the last release, **DPS8M R3.0.1**, but is subject to change until the actual debut of **DPS8M R3.0.2**.

| <u>Platform</u> | <u>R3.0.1</u>&nbsp;<u>Target</u>/<u>Toolchain</u>&nbsp;<u>Details</u> | <u>R3.0.2</u>&nbsp;<u>Target</u>/<u>Toolchain</u>&nbsp;<u>Details</u> |
|-:|:-|:-|
| <b><u>IBM</u></b>&nbsp;<b><u>AIX</u></b> | AIX&nbsp;7.2, POWER6, GCC&nbsp;12.2.1, IBM&nbsp;XL&nbsp;C/C++&nbsp;V16.1 | AIX&nbsp;7.2, POWER7, GCC&nbsp;12.2.1, IBM&nbsp;Open&nbsp;XL&nbsp;C/C++&nbsp;V17.1.2 |
| <b><u>IBM</u></b>&nbsp;<b><u>i</u></b>&nbsp;<b><u>(OS</u>/<u>400)</u></b> | (*not previously supported*) | IBM&nbsp;i&nbsp;V7R5&nbsp;(PASE), POWER9, GCC&nbsp;10.5 |
| <b><u>Android</u></b> | Android&nbsp;7, NDK&nbsp;r25b, Clang&nbsp;14.0.7 | Android&nbsp;7, NDK&nbsp;r27b, Clang&nbsp;18.0.2 |
| <b><u>FreeBSD</u></b> | FreeBSD&nbsp;13.1, GCC&nbsp;12.2.1 | FreeBSD&nbsp;14.1, Clang&nbsp;19.1.0 |
| <b><u>NetBSD</u></b> | NetBSD&nbsp;9.3, GCC&nbsp;12.2.1, Clang&nbsp;16.0.6 | NetBSD&nbsp;10, GCC&nbsp;14.2.1, Clang&nbsp;17.0.6 |
| <b><u>OpenBSD</u></b> | OpenBSD&nbsp;7.2, Clang&nbsp;13 | OpenBSD&nbsp;7.5, Clang&nbsp;16.0.6 |
| <b><u>DragonFly</u></b>&nbsp;<b><u>BSD</u></b> | DragonFly&nbsp;BSD&nbsp;6.4, Clang&nbsp;16.0.6 | DragonFly&nbsp;BSD&nbsp;6.4, Clang&nbsp;19.1.0 |
| <b><u>Haiku</u></b> | Haiku&nbsp;R1/beta4, GCC&nbsp;11.3 | Haiku&nbsp;R1/beta5, GCC&nbsp;13.3 |
| <b><u>Linux</u></b> | Linux&nbsp;2.6.32, GCC&nbsp;12.2.1 | Linux&nbsp;3.10, GCC&nbsp;14.2.1 |
| <b><u>macOS</u></b> | macOS&nbsp;10.13, SDK&nbsp;13.3, Clang&nbsp;16.0.6 | macOS&nbsp;11, SDK&nbsp;15.1, Clang&nbsp;19.1.0 |
| <b><u>Solaris</u></b> | Solaris&nbsp;11.4, GCC&nbsp;12.2.1 | Solaris 11.4&nbsp;SRU42, GCC&nbsp;14.2.1 |
| <b><u>illumos</u></b> | OpenIndiana&nbsp;8/22, GCC&nbsp;12.2.1 | OpenIndiana&nbsp;8/24, GCC&nbsp;14.2.1 |
| <b><u>SerenityOS</u></b> | SerenityOS&nbsp;8/23, GCC&nbsp;13.1 | SerenityOS&nbsp;8/24, GCC&nbsp;13.2 |
| <b><u>Windows</u></b> | Windows&nbsp;8, GCC&nbsp;12.2, Clang&nbsp;17 | Windows&nbsp;10, GCC&nbsp;14.2.1, Clang&nbsp;19.1.0 |

## Platform notes

* The platform support as described in the above table *mostly* aligns with [libuv's supported platforms](https://github.com/libuv/libuv/blob/v1.x/SUPPORTED_PLATFORMS.md).

  * We *try* to maintain support for systems somewhat older than what is officially supported by libuv v1.x, but eventually, the added maintenance burden necessitates moving on.
  * We normally drop support for end-of-life operating systems, but practical exceptions are made.

* **macOS** systems: <u>macOS 11 (Big Sur) and later is *officially* supported</u> for both ARM64 (Apple Silicon) and Intel machines, but macOS 10.13 (High Sierra) *should* work on Intel models, as this is the minimum deployment target supported by SDK 15.1.

* **Linux** systems: <u>Linux kernel 3.10.0 and later is *officially* supported</u>, but Linux kernel 3.2.0 *should* work for most architectures.  (For **R3.0.1** and earlier, Linux 2.6.32 was the target.)  All Linux binaries are built using static linking and utilize [musl-libc](https://www.musl-libc.org/) to ensure compatibility with *all* Linux distributions, depending *only* on the Linux kernel system call interface.

* **Windows** systems: <u>We *officially* support Windows 10 and later.</u>
  * Binaries for ARM64 and ARMv7 are built with Clang against UCRT using LLVM-MinGW, targeting Windows 10.
  * Binaries for x86_64 are built with GCC against UCRT using 64-bit MinGW-w64, targeting Windows 10.
  * Binaries for x86 are built with GCC against MSVCRT using 32-bit MinGW-w64, targeting Windows 10.
  []()

  []()
  * **DPS8M R3.0.2** will *most* *likely* be the final release providing binaries built against MSVCRT.

## Questions?

If you have any comments or questions about the upcoming release, feel free to [contact us](https://dps8m.gitlab.io/dps8m/Community/).

—&nbsp;*Jeffrey H. Johnson*
