---
title: Multics + AS400：DPS8M on IBM PASE for i (OS/400)
type: posts
date: 2024-02-07
tags:
  - IBM i
  - Portability
authors:
  - 'Jeffrey H. Johnson'
resources:
  - name: DPS8M on IBM i (PASE)
    src: "images/DPS8M.png"
    title: Multics MR12.8 on DPS8M on PASE for IBM i 7.5 (OS/400)
  - name: SHOW VERSION
    src: "images/SHOW_VERSION.png"
    title: '"DPS8M/400"'
  - name: SHOW PROM
    src: "images/SHOW_PROM.png"
    title:
  - name: SHOW BUILD
    src: "images/SHOW_BUILD.png"
    title:
  - name: Call Stack 1
    src: "images/Call_Stack_1.png"
    title:
  - name: Call Stack 2
    src: "images/Call_Stack_2.png"
    title:
  - name: EDTF1
    src: "images/EDTF1.png"
    title:
  - name: EDTF2
    src: "images/EDTF2.png"
    title:
  - name: QP2SHELL
    src: "images/QP2SHELL.png"
    title: QP2SHELL：IBM PASE for i, the AIX-like environment
  - name: AS/400 Then
    src: "images/AS400.jpg"
    title: IBM AS/400 (B60) in 1988
  - name: AS/400 Now
    src: "images/POWER10.jpg"
    title: IBM "AS400" (POWER10) in 2024
---
<!-- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!-- Copyright (c) 2016-2024 Jeffrey H. Johnson <trnsz@pobox.com> -->

Finally, you can run dozens of multiprocessing **Multics** instances along side your mission-critical **IBM AIX** (PASE) and **IBM i** (OS/400) workloads on **IBM Power Systems** hardware!

{{< img name="SHOW VERSION" lazy="true" alt="" size="small" >}}

This is *the* virtualization solution your IT department has been waiting for… well, *perhaps* it isn't — but supporting this platform is a great demonstration of both the capabilities of the [IBM PASE for i](https://www.ibm.com/docs/en/i/7.5?topic=programming-pase-i) (*Portable Application Solutions Environment*) runtime for enabling [OSS on IBM i](https://ibm.biz/ibmioss), and the excellent compatibility and portability of the [**DPS8M**](https://dps8m.gitlab.io) simulator software.

## AS/400?

A short introduction is in order for anyone who may have stumbled here without any idea at all about what any of this is about.

When you hear "*AS400*" today, it's most likely a reference to the [IBM i](https://www.ibm.com/products/ibm-i) operating system and associated software running on [IBM Power Systems](https://www.ibm.com/power) hardware.  The current generation of IBM server hardware is both highly performing and extremely reliable, with the many of deployments achieving actual availability near seven-nines - 99.99999% - rivaled only by mainframe platforms like [Stratus](https://www.stratus.com/) and [IBM Z](https://www.ibm.com/z).

### Pre-history ...

The story of the AS400 *really* begins with the [IBM 1401](https://en.wikipedia.org/wiki/IBM_1401) (<small>more [1401 History](https://www.ibm.com/history/1401) and [1401 Information](https://ibm-1401.info/)</small>) in 1959, the first widely popular computer.  By the mid-1960's, the majority of computers in existence were IBM 1401's.  In 1964, IBM introduced the famous [System/360](https://en.wikipedia.org/wiki/IBM_System/360) *family* of computers, a series of compatible machines covering all possibles use cases, an extremely radical concept at the time.  Unfortunately (for IBM), even the low-end 360 systems were too expensive for many 1401 customers (with the 1401 itself remaining available until 1971).  What IBM needed was a [minicomputer](https://en.wikipedia.org/wiki/Minicomputer) — a *midrange* system, in IBM lingo — less costly than an entry 360, and competitive against the stubbornly popular [Honeywell 200](https://en.wikipedia.org/wiki/Honeywell_200), also released in 1964.

### IBM midrange is born

The first IBM midrange system was the [System/3](https://en.wikipedia.org/wiki/IBM_System/3) business computer released in 1969, which was made available for less than half the cost of the lowest-end [S/360‑M20](https://en.wikipedia.org/wiki/IBM_System/360_Model_20) mainframe.  The System/3 was followed by the [System/32](https://en.wikipedia.org/wiki/IBM_System/32) in 1975. This was the system that introduced the iconic [IBM 5250 terminal](https://en.wikipedia.org/wiki/IBM_5250), an improvement on the previous 2260 and 3270 models, and pioneered the fully integrated system software concept.  In 1977, the [System/34](https://en.wikipedia.org/wiki/IBM_System/34) was launched as a multi-user and multi-tasking System/32 successor, and was quickly followed by the medium-scale [System/38](https://en.wikipedia.org/wiki/IBM_System/36) in 1978 and the small-scale [System/36](https://en.wikipedia.org/wiki/IBM_System/36) in 1983.

### The AS/400

The [**AS/400**](https://en.wikipedia.org/wiki/IBM_AS/400) hardware (and OS/400 software) was created in 1988 as combination of the System/36 and System/38 product lines.  The AS/400 used a unique layered system architecture based on the "Technology Independent Machine Interface", or TIMI, which isolates the software from the underlying hardware implementation, presenting a virtual 128-bit instruction set as the abstraction.  At the actual physical hardware level, the system was built on top of a custom 48-bit CISC architecture.

{{< img name="AS/400 Then" lazy="true" alt="" size="small" >}}

The AS/400 platform evolved into the **iSeries**, which replaced the 48-bit CISC processor with a 64-bit PowerPC-based CPU, the [RS64](https://en.wikipedia.org/wiki/IBM_RS64), a superset of the [PowerPC](https://en.wikipedia.org/wiki/PowerPC) incorporating many [POWER ISA](https://en.wikipedia.org/wiki/IBM_POWER_architecture) features.  Because of the unique software design, most user applications kept working without modification or even recompilation to take advantage of the new 64-bit hardware.

### IBM Power Systems

In 2001, the [POWER4](https://en.wikipedia.org/wiki/POWER4) CPU was released, merging the RS64 and POWER lines.

The [POWER5](https://en.wikipedia.org/wiki/POWER5) CPU came in 2005, and along with it, a rebranding of the OS/400 software to **i5/OS**, associating it with the new processor. In 2006, the hardware platform was renamed to **System i**. In 2007, the System i machines, by now **IBM Power Systems** servers with specific firmware, were upgraded with [POWER6](https://en.wikipedia.org/wiki/POWER6) processors. Finally, in 2008, i5/OS was renamed to just "**IBM i**".

Whew!  Of course, not everyone is a fan of the latest branding, and many, but certainly *[not all](https://www.nicklitten.com/i-remember-the-ibm-as-400/)* (<small>[archive.today](https://archive.is/req9O), [wayback](https://web.archive.org/web/20231001183230/https://www.nicklitten.com/i-remember-the-ibm-as-400/)</small>) users today, when referring to the platform, still affectionately call it **AS400** — most often (*but not always*) [without the slash](https://www.integrativesystems.com/journey-of-the-brawny-ibm-as400/) (<small>[archive.today](https://archive.is/5m0t3), [wayback](https://web.archive.org/web/20230607044755/https://www.integrativesystems.com/journey-of-the-brawny-ibm-as400/)</small>) — and still refer to the operating system as **OS/400**.

I don't have enough space or knowledge to write a more in-depth history of the AS400 platform, so if you're interested in learning more I recommend browsing IBM's [AS/400 History](https://www.ibm.com/history/as-400) site, and reading [Tom Van Looy's](https://ctors.net/) excellent paper [IBM AS/400: A Technical Introduction](https://treasures.scss.tcd.ie/hardware/TCD-SCSS-T.20121208.068/IBM-AS400-technical-introduction.pdf) (<small>[wayback](https://web.archive.org/web/20231016214012/https://treasures.scss.tcd.ie/hardware/TCD-SCSS-T.20121208.068/IBM-AS400-technical-introduction.pdf)</small>).

### IBM i today

It's now the year 2024, 55 years after the 1969 release of the System/3, and the IBM midrange platform is still going strong, even as some disparage the system as a "dinosaur" and describe it's modern-day adherents as [cultists](https://www.infoworld.com/article/2631698/true-believers--the-biggest-cults-in-tech.html?page=7) (<small>[archive.today](https://archive.is/zqR93), [wayback](https://web.archive.org/web/20230531234051/https://www.infoworld.com/article/2631698/true-believers--the-biggest-cults-in-tech.html?page=7)</small>).

{{< img name="AS/400 Now" lazy="true" alt="" size="small" >}}

Of course, none of that should deter our audience of GE/Honeywell/Bull and Multics fanatics; after all, our beloved 600/6000-series computers originated with the [GE M-235](https://en.wikipedia.org/wiki/MISTRAM#M-236_computer) system designed for the MISTRAM project — in 1955!

The AS400, while not sharing any direct heritage with Multics, does implement features that Multics users can appreciate, such as [single-level storage](https://en.wikipedia.org/wiki/Single-level_store#System/38_and_IBM_i_design), an MRDS-like integrated relational database management system, and the goal of providing an always-on high availability computing utility.

## Porting DPS8M

**DPS8M** was ported using [**PASE for i**](https://www.ibm.com/docs/en/i/7.5?topic=programming-pase-i), a runtime environment that supports the application binary interface (ABI) of the [IBM AIX](https://www.ibm.com/products/aix) operating system, enabling many AIX binaries to run on IBM i.

{{< img name="QP2SHELL" lazy="true" alt="" size="small" >}}

AIX binaries running on PASE for i support the direct execution of POWER instructions on the hardware itself, bypassing the usual user abstraction layer.  This provides near-native performance for CPU-heavy workloads like the **DPS8M** simulator, at the expense of future-proof portability on the platform.

### ILE?

I should mention that the feasibility of an Integrated Language Environment (ILE) port utilizing [ILE C/C++](https://www.ibm.com/docs/en/i/7.5?topic=languages-c-c) and the [ASCII Runtime for IBM i](https://www.ibm.com/support/pages/ascii-runtime-ibm-i) was explored, but the idea was abandoned.

Surprisingly, this was not because of any major inadequacies of the ILE compilers and environment (at least not for our use), which provide a (*large*, *nearly complete*) subset of C99 and C++0x and some POSIX compatibility as well, but because the ease of working with PASE and the close compatibility with AIX simply outweighed any potential benefit of the longer ILE porting process.

It's also hard to argue with the near-native performance, and future maintenance efforts are expected to be minimal.

### What was needed?

Since **DPS8M** officially supports AIX (**since R3.0.0**), utilizing PASE meant that most of the porting work was already done. Indeed, adapting our source code and build procedures to support the PASE for i environment was *mostly* straightforward.  That being said, AIX isn't PASE and PASE isn't AIX, but for the subset of AIX interfaces and libraries supported, the runtime behavior of PASE seems to be at least 90% identical.  The most noticeable differences are in lower-level OS functionality, like thread and process priority manipulation, filesystem semantics, shared library locations, and memory management.

These differences are substantial enough to require **DPS8M** be built specifically for IBM i (at least at this time), rather than being able to offer a single binary package that would work on both AIX and IBM i systems.

As a prerequisite to porting **DPS8M** itself, we needed to be sure that our major third-party dependencies were supported and functioning.

  1) [**libuv**](https://libuv.org/), a portable high‑performance platform support library, with a focus on asynchronous‑I/O based on event loops, originally developed to support the Node.js® runtime,

  2) [**libsir**](https://github.com/aremmell/libsir), the "Standard Incident Reporter" library, a lightweight, cross-platform library for information distribution (which will be used extensively in the next major version of the simulator), and,

  3) [**decNumber**](https://speleotrove.com/decimal/#decNumber), an implementation of the [General Decimal Arithmetic Specification](https://speleotrove.com/decimal/).

  4) We also have an optional dependency, [**libbacktrace**](https://github.com/ianlancetaylor/libbacktrace), which does support AIX, but is non-functional in the PASE environment.  This may or may not be able to be overcome, but it isn't a showstopper.

{{< img name="SHOW BUILD" lazy="true" alt="" size="small" >}}

*libuv* is readily available [from IBM](http://ibm.biz/ibmi-rpms), having been ported as a prerequisite of Node.js, which IBM supports on i.  The RPM package can be installed with `dnf` or `yum` from PASE and has been used to successfully build the simulator.  It can also be extracted and used to facilitate cross-compilation from Linux or AIX systems.  Our built-in libuv-builder targets (`gmake libuvrel`, `gmake libuvdev`) can be be used when compiling on PASE using GCC 10.3.0 (or later) or IBM XL C/C++ V16.1 (or later) from within the PASE environment.  I've not yet tested using IBM Open XL C/C++, the new LLVM-based toolchain, but it would likely work as well.

*decNumber* is highly portable and entirely independent of most OS specifics.  It doesn't even need stdio.  While **DPS8M** includes an optimized subset of this library (implementing only required functionality) that required no modifications, the base PASE for i environment conveniently includes this library and associated header files as well.

*libsir* required more significant modifications which are available in the current git `master` branch, and will be part of the the upcoming v2.2.5 release.

### Impressions

{{< img name="DPS8M on IBM i (PASE)" lazy="true" alt="" size="small" >}}

I was impressed with the overall development and debugging experience on IBM i.  Anyone who is comfortable with IBM AIX should feel at home in PASE.  Just imagine a slightly "weirder" (or perhaps "quirky") subset of AIX.  The PASE environment provides the AIX version of the `dbx` debugger, which I have a strong preference for, and the `gdb` debugger is also available and seems to work well, although I only tested it in conjunction with `gcc`.  If you prefer a GUI, there is the (*Java-based*) [IBM i Debugger](https://www.ibm.com/docs/en/i/7.5?topic=tools-i-debugger) which provides specific support for the PASE environment as well as the ILE.

|  |  |
| -|- |
| {{< img name="Call Stack 1" lazy="true" alt="" size="small" >}} | {{< img name="Call Stack 2" lazy="true" alt="" size="small" >}} |

For the "classic" side of i (accessed via the 5250 interface), there are debugging tools available that proved *very* helpful for this project. These include screens where you can easily watch real-time stack traces, see thread details, and monitor job status.  I even used the `EDTF` editing facility - very strong `XEDIT` vibes - and although it is very basic, it was more than sufficient for quick modifications and is, of course, instantly responsive, even when the IBM i host is lagging (or highly loaded), due to the use the block/screen-oriented 5250 protocol.  This came in handy a few times when my connection quality was less than ideal.  PASE programs can also call ILE (and vice versa) so porting with PASE is a good way to ease into working with ILE and IBM i in general.

|  |  |
| -|- |
| {{< img name="EDTF1" lazy="true" alt="" size="small" >}} | {{< img name="EDTF2" lazy="true" alt="" size="small" >}} |

Don't let the green screen scare you away from the platform!

There is [IBM Rational Developer for i](https://www.ibm.com/products/rational-developer-for-i) (for Eclipse fans) and [Code for IBM i](https://marketplace.visualstudio.com/items?itemName=HalcyonTechLtd.code-for-ibmi) (for VSCode fans). You can work with the integrated database with [IBM i Access ODBC](https://ibmi-oss-docs.readthedocs.io/en/latest/odbc/) from Linux, Windows, and macOS desktops, and use [IBM i ACS](https://www.ibm.com/support/pages/ibm-i-access-client-solutions) to access IBM i completely from a web browser (<small>yuck</small>).

The unexpected thing is that I absolutely didn't hate this system.  In fact, I liked it.  I liked it a lot.  Probably more than I should have.  I'm going to continue to learn more about the IBM i platform (outside of the PASE environment) and look forward to putting some i skills on the resume.

Modern IBM i powers many multibillion dollar businesses today (like Costco, with $245B in annual revenue).  Now they can run Multics as well! **`:-)`**

## Let's have it!

While I'm not going to promise IBM i builds in CI just yet, the changes necessary to build on IBM PASE for i should land in the `master` git repo in the next few days, and will be buildable under IBM PASE for i (that is, on OS/400) using GCC 10.3.0 (or later) or IBM XL C/C++ 16.1 (or later), and requires a minimum libuv version of 1.42.0.  We've only tested IBM i 7.5, but everything should work on IBM i 7.4 as well.  Cross-compilation is possible and will be documented in the near future.

NOTE: IBM i 7.3 and 7.2 *might* work, but are unsupported by us, so your mileage may vary. IBM i 7.1 and below will almost certainly *never* work. **DPS8M** has always targeted AIX 7 and the PASE environment on 7.1 only offers parity with AIX 6.1.

These binaries are compiled for POWER9, the minimum supported processor level for IBM i 7.5.  The `prt2pdf` and `punutil` utilities are included.  The `punutil` utility requires the `popt` package to be installed via RPM.

### Downloads

* [**dps8m-os400-75-p9-20240207.tar.gz**](https://gitlab.com/dps8m/dps8m/-/wikis/uploads/43e0b97d4aad46a3e11522c664b28376/dps8m-os400-75-p9-20240207.tar.gz)
  * sha256sum: `799c9ab49fdaab3b5d7112263cbbdf4e12a881a5ed394a8d546828bb34fc3c54`

<small>The software provided is experimental and distributed "**AS IS**", **WITHOUT WARRANTY OF ANY KIND**, under the terms of the **ICU License**.</small>

## Acknowledgements

**A big thank you is due to [Holger Scherer of POWERbunker, RZKH GmBh](https://www.rzkh-gmbh.de/) for donating the resources that helped make this development possible.**

**If you need commercial IBM i hosting, don't hesitate to get in touch with them.  You can also try out their [PUB400](https://pub400.com/) service for limited free public access to IBM i for non-commercial use.**

—&nbsp;[*Jeffrey H. Johnson*](mailto:trnsz@pobox.com)
